import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ConfigModule } from './config/config.module';
import { dbLink } from './config/constants';
import { BookkeepingModule } from './bookkeeping/bookkeeping.module';
import { WalletsModule } from './wallets/wallets.module';
import { TransactionModule } from './transaction/transaction.module';
import { HelperModule } from './helper/helper.module';
import { ResetModule } from './reset/reset.module';

@Module({
	imports: [
		MongooseModule.forRoot(dbLink, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
			useCreateIndex: true,
		}),
		ConfigModule,
		AuthModule,
		UsersModule,
		BookkeepingModule,
		WalletsModule,
		TransactionModule,
		HelperModule,
		ResetModule,
	],
	controllers: [AppController],
	providers: [
		AppService,
	],
})
export class AppModule { }
