import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';

@Injectable()
export abstract class BaseService<T> {
	protected parentIdFieldName: string;

	constructor(
		protected model: Model<any>,
	) { }

	public async createNew(element: Partial<T>): Promise<T> {
		const newElement = new this.model(element);

		return newElement.save();
	}

	public async getAllByParentId(parentId: string): Promise<T[] | null> {
		const parentIdFieldName = this.parentIdFieldName;
		const elements = await this.model.find({ [parentIdFieldName]: parentId });

		return elements ? elements : null;
	}

	public async getById(_id: string): Promise<T | null> {
		const element = await this.model.findOne({ _id });

		return element ? element : null;
	}

	public async edit(_id: string, parentId: string, updatedElement: Partial<T>): Promise<T> {
		const parentIdFieldName = this.parentIdFieldName;
		return this.model.findOneAndUpdate({ _id, [parentIdFieldName]: parentId }, updatedElement, (error, updatedElement) => {
			if (error) {
				return null;
			}
			return updatedElement;
		});
	}

	public async delete(_id: string, parentId: string): Promise<T> {
		const parentIdFieldName = this.parentIdFieldName;
		const element = await this.model.findOne({ _id, [parentIdFieldName]: parentId });

		return element ? element.deleteOne() : null;
	}

	public async deleteAllByParentId(parentId: string): Promise<boolean> {
		const parentIdFieldName = this.parentIdFieldName;
		return this.model.deleteMany({ [parentIdFieldName]: parentId }, (err) => !err ? true : false);
	}
}
