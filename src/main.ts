import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';
import express = require('express');

async function bootstrap(): Promise<any> {
	const app = await NestFactory.create(AppModule, {
		logger: true,
	});

	app.use(helmet());
	app.use(
		rateLimit({
			windowMs: 15 * 60 * 1000, // 15 minutes
			max: 100, // limit each IP to 100 requests per windowMs
		}),
	);
	(app as any).setViewEngine('hbs');
	app.use(express.static(__dirname + '/views'));

	await app.listen(3000);
}
bootstrap();
