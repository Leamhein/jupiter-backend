import { Controller, Res, Req, Query, Get, Render, Put, Body, HttpException, HttpStatus } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { Response } from 'express';
import { join } from 'path';

@Controller('reset')
export class ResetController {
	constructor(
		private userService: UsersService,
	) { }

	@Get()
	public root(@Res() res: Response, @Query('token') token: string, @Req() _req: any): void {
		this.getViewName(token).then(pageName =>
			res.render(
				pageName,
			)).catch(() => res.render(
				join(__dirname, '../views', 'expired/expired.hbs'),
			));
	}

	private async getViewName(token: string): Promise<string> {
		const isResetPasswordTokenValid = !!(await this.userService.findUserByPasswordToken(token));

		if (isResetPasswordTokenValid) {
			return join(__dirname, '../views', 'reset/reset.hbs');
		}

		return join(__dirname, '../views', 'expired/expired.hbs');
	}

	@Put()
	public async updateUserPassword(
		@Res() res: Response,
		@Query('token') token: string,
		@Body('password') password: string,
	): Promise<void> {
		const user = await this.userService.findUserByPasswordToken(token);

		if (!user) {
			throw new HttpException(user, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		const passwordHash = await this.userService.getHash(password);

		this.userService.editUser(user._id, {
			passwordHash,
			resetPasswordExpires: null,
			resetPasswordToken: null,
		}).then(
			() => res.status(HttpStatus.OK).send('Success'),
		).catch(err => {
			throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR);
		});
	}
}
