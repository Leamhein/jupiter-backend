import { Module } from '@nestjs/common';
import { ResetController } from './reset.controller';
import { UsersModule } from '../users/users.module';

@Module({
	imports: [UsersModule],
	controllers: [ResetController],
})
export class ResetModule {}
