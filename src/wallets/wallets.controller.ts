import { Controller, UseGuards, Post, Res, Body, HttpStatus, HttpException, Get, Query, Put, Delete } from '@nestjs/common';
import { WalletsService } from './wallets.service';
import { AuthGuard } from '@nestjs/passport';
import { Wallet } from './wallets.model';
import { BookkeepingService } from '../bookkeeping/bookkeeping.service';
import { HelperService } from '../helper/helper.service';

@Controller('wallets')
export class WalletsController {
	constructor(
		private walletsService: WalletsService,
		private bookkeepingService: BookkeepingService,
		private helperService: HelperService,
	) {}

	@UseGuards(AuthGuard('jwt'))
	@Post()
	public async addWallet(@Res() res: any, @Body() body: Partial<Wallet>): Promise<Wallet> {
		const parentBookkeeping = await this.bookkeepingService.getById(body.bookkeepingId as string);
		if (!parentBookkeeping) {
			throw new HttpException('Invalid bookkeeping id', HttpStatus.BAD_REQUEST);
		}
		const newWallet = await this.walletsService.createNew({...body, bookkeepingId: parentBookkeeping._id});

		if (newWallet) {
			return res.status(HttpStatus.OK).send(newWallet);
		}

		throw new HttpException('Something wrong! Can\'t register wallet', HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@UseGuards(AuthGuard('jwt'))
	@Get('all')
	public async getAllWalletsByBookkeeping(@Res() res: any, @Query('bookkeepingId') bookkeepingId: string): Promise<Wallet[] | []> {
		if (!this.helperService.isIdValid(bookkeepingId)) {
			throw new HttpException('Invalid bookkeeping id', HttpStatus.BAD_REQUEST);
		}
		const walletsByBookkeeping = await this.walletsService.getAllByParentId(bookkeepingId);

		if (walletsByBookkeeping) {
			return res.status(HttpStatus.OK).send(walletsByBookkeeping);
		}

		throw new HttpException('No wallets found', HttpStatus.NOT_FOUND);
	}

	@UseGuards(AuthGuard('jwt'))
	@Get()
	public async getWalletById(@Res() res: any, @Query('id') id: string): Promise<Wallet[] | []> {
		if (!this.helperService.isIdValid(id)) {
			throw new HttpException('Invalid wallet id', HttpStatus.BAD_REQUEST);
		}
		const wallet = await this.walletsService.getById(id);

		if (wallet) {
			return res.status(HttpStatus.OK).send(wallet);
		}

		throw new HttpException('No wallet found', HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@UseGuards(AuthGuard('jwt'))
	@Put()
	public async editWallet(
		@Res() res: any,
		@Body('id') id: string,
		@Body('bookkeepingId') bookkeepingId: string,
		@Body() body: Partial<Wallet>,
	): Promise<Wallet[]> {
		const editedWallet = await this.walletsService.edit(id, bookkeepingId, body);

		if (editedWallet) {
			return res.status(HttpStatus.OK).send(editedWallet);
		}

		throw new HttpException('Something wrong! Can\'t find wallet', HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@UseGuards(AuthGuard('jwt'))
	@Delete()
	public async deleteWallet(@Res() res: any, @Body('id') id: string, @Body('bookkeepingId') bookkeepingId: string): Promise<any> {
		const isDeleted = await this.walletsService.delete(id, bookkeepingId);

		if (isDeleted) {
			return res.status(HttpStatus.OK).send('Successfully deleted');
		}

		throw new HttpException('Something wrong! Can\'t find wallet', HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
