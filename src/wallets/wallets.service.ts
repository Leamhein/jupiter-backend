import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Wallet } from './wallets.model';
import { BaseService } from '../models/base-service';

@Injectable()
export class WalletsService extends BaseService<Wallet> {
	protected parentIdFieldName = 'bookkeepingId';
	constructor(
		@InjectModel('Wallets') readonly walletsModel: Model<Wallet>,
	) {
		super(walletsModel);
	}
}
