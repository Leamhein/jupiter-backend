import { Bookkeeping } from '../bookkeeping/bookkeeping.model';

export class Wallet {
	public bookkeepingId: string | Bookkeeping;
	public title: string;
	public balance: number;
	public icon: string;
	public _id: string | null = null;

	constructor(params: Partial<Wallet>) {
		Object.assign(this, params);
	}
}
