import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { WalletsController } from './wallets.controller';
import { WalletsService } from './wallets.service';
import { WalletsSchema } from './wallets.schema';
import { BookkeepingModule } from '../bookkeeping/bookkeeping.module';
import { HelperModule } from '../helper/helper.module';

@Module({
	imports: [
		MongooseModule.forFeature([{ name: 'Wallets', schema: WalletsSchema }]),
		forwardRef(() => BookkeepingModule),
		HelperModule,
	],
	controllers: [WalletsController],
	providers: [WalletsService],
	exports: [
		WalletsService,
	],
})
export class WalletsModule { }
