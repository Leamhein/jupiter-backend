import * as mongoose from 'mongoose';

export const WalletsSchema = new mongoose.Schema({
	bookkeepingId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Bookkeeping',
	},
	title: {
		type: String,
		required: true,
	},
	balance: {
		type: Number,
		required: true,
	},
	icon: {
		type: String,
		required: true,
	},
});
