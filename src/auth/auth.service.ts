import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { NewUser, ProtectedUser } from '../users/users.model';
import { UsersService } from '../users/users.service';
import { TokenResponse } from './auth.model';
import { MailService } from '../mail/mail.service';
import { TWELVE_HOURS } from '../config/constants';

@Injectable()
export class AuthService {
	constructor(
		private readonly usersService: UsersService,
		private readonly jwtService: JwtService,
		private readonly mailService: MailService,
	) { }

	public login(user: any): TokenResponse {
		const payload = {
			username: user._doc.username,
			sub: user._doc._id,
		};

		return new TokenResponse(this.jwtService.sign(payload), payload.sub);
	}

	public async validateUser(username: string, password: string): Promise<Partial<NewUser | null>> {
		const user = await this.usersService.getUserByUsername(username);
		if (user) {
			if (await this.usersService.compareHash(password, user.passwordHash)) {
				const { passwordHash, ...result } = user;

				return result;
			}

			throw new HttpException('Invalid credentials', HttpStatus.FORBIDDEN);
		}

		throw new HttpException('No user found', HttpStatus.NOT_FOUND);
	}

	public async restorePassword(email: string, host: string): Promise<string> {
		const user = await this.usersService.getUserByEmail(email);

		if (!user) {
			throw new HttpException('Password reset failed. Email not found.', HttpStatus.NOT_FOUND);
		}

		const userWithRestoreToken = await this.generateResetPasswordToken(user);
		const updatedUser = await this.usersService.editUser(userWithRestoreToken._id, userWithRestoreToken);

		if (!updatedUser) {
			throw new HttpException('Password reset failed.', HttpStatus.INTERNAL_SERVER_ERROR);
		}

		const subject = 'Flow Password Reset';
		const messageText = `You are receiving this because you (or someone else) have requested the reset of the password for your account.
									Please click on the following link, or paste this into your browser to complete the process:
									http://${host}/reset?token=${user.resetPasswordToken}
									If you did not request this, please ignore this email and your password will remain unchanged.`;

		return this.mailService.sendEmail(user.email, subject, messageText);
	}

	private async generateResetPasswordToken(user: Partial<ProtectedUser>): Promise<Partial<ProtectedUser>> {
		const saltRounds = 10;
		const userWithToken = user;
		const resetPasswordToken: string = await new Promise((resolve, reject) => {
			bcrypt.hash(user.email, saltRounds, (err, hash) => {
				if (err) {
					reject(err);
				}
				resolve(hash);
			});
		});

		userWithToken.resetPasswordToken = resetPasswordToken;
		userWithToken.resetPasswordExpires = Date.now() + TWELVE_HOURS;

		return userWithToken;
	}
}
