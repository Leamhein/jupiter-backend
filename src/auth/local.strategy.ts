import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ProtectedUser } from '../users/users.model';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
	constructor(
		private readonly authService: AuthService,
	) {
		super();
	}

	public async validate(username: string, password: string): Promise<Partial<ProtectedUser> | UnauthorizedException> {
		const user = await this.authService.validateUser(username, password);

		return user ? user : new UnauthorizedException();
	}
}
