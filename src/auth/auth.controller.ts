import { Controller, UseGuards, Post, Request, Body, HttpStatus, HttpException, Response } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TokenResponse } from './auth.model';
import { AuthService } from './auth.service';
import { NewUser } from '../users/users.model';
import { UsersService } from '../users/users.service';

@Controller('auth')
export class AuthController {
	constructor(
		private readonly authService: AuthService,
		private readonly usersService: UsersService,
	) { }

	@UseGuards(AuthGuard('local'))
	@Post('login')
	public async login(@Request() req: any): Promise<TokenResponse> {
		return this.authService.login(req.user);
	}

	@Post('register')
	public async registerUser(@Response() res: any, @Body() body: Partial<NewUser>): Promise<any> {
		if (!(body && body.username && body.password && body.email)) {
			throw new HttpException('Username, password and email are required!', HttpStatus.FORBIDDEN);
		}

		const isUserExist = !!await this.usersService.getUserByUsername(body.username) || !!await this.usersService.getUserByEmail(body.email);

		if (isUserExist) {
			throw new HttpException('User with such username or email already exist', HttpStatus.CONFLICT);
		} else {
			const user = await this.usersService.createUser(body);
			if (user) {
				return res.status(HttpStatus.OK).send('Successfully registered!');
			}

			throw new HttpException('Something wrong! Can\'t register user', HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Post('restore')
	public async restorePassword(@Request() req: any, @Response() res: any, @Body('email') email: string): Promise<any> {
		const result = await this.authService.restorePassword(email, req.headers.host);

		return res.status(HttpStatus.OK).send(result);
	}
}
