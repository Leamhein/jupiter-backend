export class TokenResponse {
	constructor(
		public accessToken: string,
		public userId: string,
	) {}
}
