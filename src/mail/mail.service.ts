import { Injectable } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
const nodemailer = require('nodemailer');
const env = new ConfigService();

@Injectable()
export class MailService {
	private transporter = nodemailer.createTransport({
		host: env.getEnvVariable('emailHost'),
		port: env.getEnvVariable('emailPort'),
		secure: false,
		auth: {
			user: env.getEnvVariable('email'),
			pass: env.getEnvVariable('emailPass'),
		},
		tls: {
			rejectUnauthorized: false,
		},
	});

	public async sendEmail(email: string, subject: string, text: string): Promise<string> {
		const mailOptions = {
			to: email,
			from: env.getEnvVariable('email'),
			subject,
			text,
		};

		return new Promise((resolve, reject) => {
			this.transporter.sendMail(mailOptions, (err) => {
				if (err) {
					reject(err);
				}
				resolve(`An e-mail has been sent to ${email} with further instructions.`);
			});
		});
	}
}
