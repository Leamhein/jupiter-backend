import { Injectable } from '@nestjs/common';
import { Types } from 'mongoose';

@Injectable()
export class HelperService {
	public isIdValid(id: string): boolean {
		return Types.ObjectId.isValid(id);
	}
}
