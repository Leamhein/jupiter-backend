import { Test, TestingModule } from '@nestjs/testing';
import { WalletCalculatorService } from './wallet-calculator.service';

describe('WalletCalculatorService', () => {
	let service: WalletCalculatorService;

	beforeEach(async() => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [WalletCalculatorService],
		}).compile();

		service = module.get<WalletCalculatorService>(WalletCalculatorService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});
});
