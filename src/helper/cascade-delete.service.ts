import { Injectable } from '@nestjs/common';
import { WalletsService } from 'src/wallets/wallets.service';
import { TransactionService } from 'src/transaction/transaction.service';
import { BookkeepingService } from 'src/bookkeeping/bookkeeping.service';

@Injectable()
export class CascadeDeleteService {
	public static bookkeepingService: BookkeepingService;
	public static walletService: WalletsService;
	public static transactionService: TransactionService;

	public static async deleteWalletsAndTransactions(bookkeepingId: string): Promise<void> {
		const wallets = await CascadeDeleteService.walletService.getAllByParentId(bookkeepingId);

		CascadeDeleteService.walletService.deleteAllByParentId(bookkeepingId);
		if (wallets) {
			wallets.forEach(({ _id }) => CascadeDeleteService.transactionService.deleteAllByParentId(_id));
		}
	}

	public static async deleteBookkeepings(userId: string): Promise<void> {
		const bookkeepings = await CascadeDeleteService.bookkeepingService.getAllByParentId(userId);

		CascadeDeleteService.bookkeepingService.deleteAllByParentId(userId);
		if (bookkeepings) {
			bookkeepings.forEach(bookkeeping => CascadeDeleteService.deleteWalletsAndTransactions(bookkeeping._id));
		}
	}
 }
