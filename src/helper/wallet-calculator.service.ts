import Big from 'big.js';
import { Injectable } from '@nestjs/common';
import { WalletsService } from 'src/wallets/wallets.service';
import { Transaction } from '../transaction/transaction.model';

@Injectable()
export class WalletCalculatorService {
	public static walletService: WalletsService;
	public static async updateWallet(transaction: Transaction): Promise<void> {
		const walletToUpdate = await WalletCalculatorService.walletService.getById(transaction.walletId as string);
		const currentWalletBalance = Big(walletToUpdate.balance);
		const updatedWalletBalance = parseFloat(currentWalletBalance.plus(transaction.amount).toFixed());
		const updatedWallet = { balance: updatedWalletBalance };
		const { _id, bookkeepingId } = walletToUpdate;

		WalletCalculatorService.walletService.edit(_id, bookkeepingId as string, updatedWallet);
	}
}
