import { Module } from '@nestjs/common';
import { HelperService } from './helper.service';
import { WalletCalculatorService } from './wallet-calculator.service';
import { CascadeDeleteService } from './cascade-delete.service';

@Module({
	providers: [
		HelperService,
		WalletCalculatorService,
		CascadeDeleteService,
	],
	exports: [
		HelperService,
		WalletCalculatorService,
		CascadeDeleteService,
	],
})
export class HelperModule {}
