import { Test, TestingModule } from '@nestjs/testing';
import { CascadeDeleteService } from './cascade-delete.service';

describe('CascadeDeleteService', () => {
	let service: CascadeDeleteService;

	beforeEach(async() => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [CascadeDeleteService],
		}).compile();

		service = module.get<CascadeDeleteService>(CascadeDeleteService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});
});
