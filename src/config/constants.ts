import { ConfigService } from './config.service';

const env = new ConfigService();

export const jwtConstants = {
	secret: env.getEnvVariable('jwtSecretKeyAlpha'),
};

// tslint:disable-next-line: max-line-length
export const dbLink = `mongodb+srv://${env.getEnvVariable('dbUser')}:${env.getEnvVariable('dbPass')}@glidecluster-ixj4i.mongodb.net/Glide?retryWrites=true`;

export const TWELVE_HOURS = 43200000;
