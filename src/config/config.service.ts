import * as dotenv from 'dotenv';
import * as fs from 'fs';
import { IEnvConfig } from './config.model';

export class ConfigService {
	private readonly envConfig: IEnvConfig;

	constructor() {
		if (
			process.env.NODE_ENV === 'production' ||
			process.env.NODE_ENV === 'staging'
		) {
			this.envConfig = process.env;
		} else {
			this.envConfig = dotenv.parse(fs.readFileSync('.env'));
		}
	}

	public getEnvVariable(key: string): string {
		return this.envConfig[key];
	}
}
