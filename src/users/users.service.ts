import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { NewUser, ProtectedUser } from './users.model';

@Injectable()
export class UsersService {
	private saltRounds = 10;

	constructor(
		@InjectModel('User') private readonly userModel: Model<ProtectedUser>,
	) {}

	public async createUser(user: Partial<NewUser>): Promise<Partial<ProtectedUser>> {
		const passwordHash = await this.getHash(user.password);
		const newUser = new this.userModel(new ProtectedUser(user, passwordHash));

		return newUser.save();
	}

	public async getHash(password: string): Promise<string> {
		return bcrypt.hash(password, this.saltRounds);
	}

	public async getUserByUsername(username: string): Promise<Partial<ProtectedUser> | null> {
		const user = await this.userModel.findOne({ username });

		return user ? user : null;
	}

	public async getUserByEmail(email: string): Promise<Partial<ProtectedUser> | null> {
		const user = await this.userModel.findOne({ email });

		return user ? user : null;
	}

	public async getUserById(_id: string): Promise<Partial<ProtectedUser> | null> {
		const user = await this.userModel.findOne({ _id });

		return user ? user : null;
	}

	public async compareHash(password: string | undefined, hash: string | undefined): Promise<boolean> {
		return bcrypt.compare(password, hash);
	}

	public async editUser(_id: string, user: Partial<ProtectedUser>): Promise<Partial<ProtectedUser>> {
		return this.userModel.findOneAndUpdate({ _id }, user, (error, user) => {
			if (error) {
				throw new HttpException(error, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return user;
		});
	}

	public async deleteUser(_id: string): Promise<boolean> {
		const user = await this.userModel.findOne({ _id });

		return user ? user.deleteOne() : null;
	}

	public async findUserByPasswordToken(resetPasswordToken: string): Promise<ProtectedUser | null> {
		return this.userModel.findOne({ resetPasswordToken, resetPasswordExpires: { $gt: Date.now() } }, (_err, user) => {
			if (!user) {
				return null;
			}
			return user;
		});
	}
}
