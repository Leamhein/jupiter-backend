import { Controller, UseGuards, Delete, HttpStatus, HttpException, Res, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UsersService } from './users.service';

@Controller('user')
export class UsersController {
	constructor(
		private userService: UsersService,
	) {}

	@UseGuards(AuthGuard('jwt'))
	@Delete()
	public async deleteUser(@Res() res: any, @Body('id') id: string): Promise<any> {
		const isDeleted = await this.userService.deleteUser(id);

		if (isDeleted) {
			return res.status(HttpStatus.OK).send('Successfully deleted');
		}

		throw new HttpException('Something wrong! Can\'t find bookkeeping', HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
