abstract class User {
	public username: string;
	public email: string;
}

export class NewUser extends User {
	public password: string;

	constructor(params: Partial<NewUser>) {
		super();
		Object.assign(this, params);
	}
}

export class ProtectedUser extends User {
	public passwordHash: string;
	public _id: string | null = null;
	public resetPasswordToken?: string;
	public resetPasswordExpires?: number;

	constructor(params: Partial<NewUser>, passwordHash: string) {
		super();
		Object.assign(this, params);
		this.passwordHash = passwordHash;
	}
}
