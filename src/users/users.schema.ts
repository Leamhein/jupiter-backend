import * as mongoose from 'mongoose';
import { CascadeDeleteService } from '../helper/cascade-delete.service';

export const UserSchema = new mongoose.Schema({
	username: {
		type: String,
		unique: true,
		required: true,
	},
	passwordHash: {
		type: String,
		required: true,
	},
	email: {
		type: String,
		required: true,
		unique: true,
	},
	resetPasswordToken: String,
	resetPasswordExpires: Number,
});

UserSchema.post('deleteOne', { document: true, query: false }, async(user) => {
	CascadeDeleteService.deleteBookkeepings(user._id);
});
