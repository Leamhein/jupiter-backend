export class Bookkeeping {
	public userId: string;
	public title: string;
	public description: string;
	public icon: string;
	public currency: string;
	public _id: string | null = null;

	constructor(params: Partial<Bookkeeping>) {
		Object.assign(this, params);
	}
}
