import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BookkeepingSchema } from './bookkeeping.schema';
import { BookkeepingController } from './bookkeeping.controller';
import { BookkeepingService } from './bookkeeping.service';
import { UsersModule } from '../users/users.module';
import { HelperModule } from '../helper/helper.module';

@Module({
	imports: [
		MongooseModule.forFeature([{ name: 'Bookkeeping', schema: BookkeepingSchema }]),
		forwardRef(() => UsersModule),
		HelperModule,
	],
	controllers: [BookkeepingController],
	providers: [
		BookkeepingService,
	],
	exports: [BookkeepingService],
})
export class BookkeepingModule { }
