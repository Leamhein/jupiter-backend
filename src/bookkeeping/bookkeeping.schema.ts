import * as mongoose from 'mongoose';
import { CascadeDeleteService } from '../helper/cascade-delete.service';
import { Bookkeeping } from './bookkeeping.model';

export const BookkeepingSchema = new mongoose.Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
	},
	title: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		required: true,
	},
	currency: {
		type: String,
		required: true,
	},
	icon: {
		type: String,
		required: true,
	},
});

BookkeepingSchema.post('deleteOne', { document: true, query: false }, async(bookkeeping: Bookkeeping) => {
	CascadeDeleteService.deleteWalletsAndTransactions(bookkeeping._id);
});
