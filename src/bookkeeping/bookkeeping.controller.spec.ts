import { Test, TestingModule } from '@nestjs/testing';
import { BookkeepingController } from './bookkeeping.controller';

describe('Bookkeeping Controller', () => {
	let controller: BookkeepingController;

	beforeEach(async() => {
		const module: TestingModule = await Test.createTestingModule({
			controllers: [BookkeepingController],
		}).compile();

		controller = module.get<BookkeepingController>(BookkeepingController);
	});

	it('should be defined', () => {
		expect(controller).toBeDefined();
	});
});
