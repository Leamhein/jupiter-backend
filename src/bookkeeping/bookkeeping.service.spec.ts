import { Test, TestingModule } from '@nestjs/testing';
import { BookkeepingService } from './bookkeeping.service';

describe('BookkeepingService', () => {
	let service: BookkeepingService;

	beforeEach(async() => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [BookkeepingService],
		}).compile();

		service = module.get<BookkeepingService>(BookkeepingService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});
});
