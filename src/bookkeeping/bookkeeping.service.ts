import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Bookkeeping } from './bookkeeping.model';
import { BaseService } from '../models/base-service';

@Injectable()
export class BookkeepingService extends BaseService<Bookkeeping> {
	protected readonly parentIdFieldName: string = 'userId';
	constructor(
		@InjectModel('Bookkeeping') readonly bookkeepingModel: Model<Bookkeeping>,
	) {
		super(
			bookkeepingModel,
		);
	}
}
