import { Controller, Post, Res, Body, UseGuards, HttpStatus, HttpException, Get, Query, Put, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Bookkeeping } from './bookkeeping.model';
import { BookkeepingService } from './bookkeeping.service';
import { UsersService } from '../users/users.service';
import { HelperService } from '../helper/helper.service';

@Controller('bookkeeping')
export class BookkeepingController {
	constructor(
		private bookkeepingService: BookkeepingService,
		private userService: UsersService,
		private helperService: HelperService,
	) { }

	@UseGuards(AuthGuard('jwt'))
	@Post()
	public async addBookkeeping(@Res() res: any, @Body() body: Partial<Bookkeeping>): Promise<Bookkeeping> {
		const user = await this.userService.getUserById(body.userId);
		if (!user) {
			throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
		}
		const newBookkeeping = await this.bookkeepingService.createNew({...body, userId: user._id});

		if (newBookkeeping) {
			return res.status(HttpStatus.OK).send(newBookkeeping);
		}

		throw new HttpException('Something wrong! Can\'t register bookkeeping', HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@UseGuards(AuthGuard('jwt'))
	@Get('all')
	public async getAllBookkeepingsByUser(@Res() res: any, @Query('userId') userId: string): Promise<Bookkeeping[] | []> {
		if (!this.helperService.isIdValid(userId)) {
			throw new HttpException('Invalid user id', HttpStatus.BAD_REQUEST);
		}
		const bookkeepingsByUser = await this.bookkeepingService.getAllByParentId(userId);

		if (bookkeepingsByUser) {
			return res.status(HttpStatus.OK).send(bookkeepingsByUser);
		}

		throw new HttpException('No bookkeepings found', HttpStatus.NOT_FOUND);
	}

	@UseGuards(AuthGuard('jwt'))
	@Get()
	public async getBookkeepingById(@Res() res: any, @Query('id') id: string): Promise<Bookkeeping[] | []> {
		if (!this.helperService.isIdValid(id)) {
			throw new HttpException('Invalid bookkeeping id', HttpStatus.BAD_REQUEST);
		}
		const bookkeeping = await this.bookkeepingService.getById(id);

		if (bookkeeping) {
			return res.status(HttpStatus.OK).send(bookkeeping);
		}

		throw new HttpException('No bookkeeping found', HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@UseGuards(AuthGuard('jwt'))
	@Put()
	public async editBookkeeping(
		@Res() res: any,
		@Body('id') id: string,
		@Body('userId') userId: string,
		@Body() body: Partial<Bookkeeping>,
	): Promise<Bookkeeping[]> {
		const editedBookkeeping = await this.bookkeepingService.edit(id, userId, body);

		if (editedBookkeeping) {
			return res.status(HttpStatus.OK).send(editedBookkeeping);
		}

		throw new HttpException('Something wrong! Can\'t find bookkeeping', HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@UseGuards(AuthGuard('jwt'))
	@Delete()
	public async deleteBookkeeping(@Res() res: any, @Body('id') id: string, @Body('userId') userId: string): Promise<any> {
		const isDeleted = await this.bookkeepingService.delete(id, userId);

		if (isDeleted) {
			return res.status(HttpStatus.OK).send('Successfully deleted');
		}

		throw new HttpException('Something wrong! Can\'t find bookkeeping', HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
