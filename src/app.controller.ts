import { Controller } from '@nestjs/common';
import { WalletsService } from './wallets/wallets.service';
import { WalletCalculatorService } from './helper/wallet-calculator.service';
import { CascadeDeleteService } from './helper/cascade-delete.service';
import { TransactionService } from './transaction/transaction.service';
import { BookkeepingService } from './bookkeeping/bookkeeping.service';

@Controller()
export class AppController {
	constructor(
		private bookkeeping: BookkeepingService,
		private walletService: WalletsService,
		private transactionsService: TransactionService,
	) {
		WalletCalculatorService.walletService = this.walletService;
		CascadeDeleteService.bookkeepingService = this.bookkeeping;
		CascadeDeleteService.walletService = this.walletService;
		CascadeDeleteService.transactionService = this.transactionsService;
	}
}
