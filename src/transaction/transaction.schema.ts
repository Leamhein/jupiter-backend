import * as mongoose from 'mongoose';
import { WalletCalculatorService } from '../helper/wallet-calculator.service';

export const TransactionSchema = new mongoose.Schema({
	walletId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Wallets',
	},
	category: {
		type: String,
		required: true,
	},
	amount: {
		type: Number,
		required: true,
	},
	date: {
		type: Number,
		required: true,
	},
	note: {
		type: String,
		required: false,
	},
});

TransactionSchema.post('save', async(transaction) => {
	WalletCalculatorService.updateWallet(transaction);
});

TransactionSchema.post('deleteOne', { document: true, query: false }, async(transaction) => {
	WalletCalculatorService.updateWallet(transaction);
});
