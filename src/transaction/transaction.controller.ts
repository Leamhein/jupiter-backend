import { Controller, UseGuards, Post, Res, Body, HttpException, HttpStatus, Get, Query, Put, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Transaction } from './transaction.model';
import { WalletsService } from '../wallets/wallets.service';
import { TransactionService } from './transaction.service';
import { HelperService } from '../helper/helper.service';

@Controller('transaction')
export class TransactionController {
	constructor(
		private walletService: WalletsService,
		private transactionService: TransactionService,
		private helperService: HelperService,
	) { }

	@UseGuards(AuthGuard('jwt'))
	@Post()
	public async addTransaction(@Res() res: any, @Body() body: Partial<Transaction>): Promise<Transaction> {
		const parentWallet = await this.walletService.getById(body.walletId as string);

		if (!parentWallet) {
			throw new HttpException('Invalid wallet id', HttpStatus.BAD_REQUEST);
		}
		const newTransaction = await this.transactionService.createNew({ ...body, walletId: parentWallet._id });

		if (newTransaction) {
			return res.status(HttpStatus.OK).send(newTransaction);
		}

		throw new HttpException('Something wrong! Can\'t register transaction', HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@UseGuards(AuthGuard('jwt'))
	@Get('all')
	public async getAllTransactionsByWallet(@Res() res: any, @Query('walletId') walletId: string): Promise<Transaction[] | []> {
		if (!this.helperService.isIdValid(walletId)) {
			throw new HttpException('Invalid wallet id', HttpStatus.BAD_REQUEST);
		}
		const walletsByBookkeeping = await this.transactionService.getAllByParentId(walletId);

		if (walletsByBookkeeping) {
			return res.status(HttpStatus.OK).send(walletsByBookkeeping);
		}

		throw new HttpException('No transactions found', HttpStatus.NOT_FOUND);
	}

	@UseGuards(AuthGuard('jwt'))
	@Get()
	public async getTransactionById(@Res() res: any, @Query('id') id: string): Promise<Transaction[] | []> {
		if (!this.helperService.isIdValid(id)) {
			throw new HttpException('Invalid transaction id', HttpStatus.BAD_REQUEST);
		}
		const transaction = await this.transactionService.getById(id);

		if (transaction) {
			return res.status(HttpStatus.OK).send(transaction);
		}

		throw new HttpException('No transactions found', HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@UseGuards(AuthGuard('jwt'))
	@Put()
	public async editTransaction(
		@Res() res: any,
		@Body('id') id: string,
		@Body('walletId') walletId: string,
		@Body() body: Partial<Transaction>,
	): Promise<Transaction[]> {
		const editedTransaction = await this.transactionService.edit(id, walletId, body);

		if (editedTransaction) {
			return res.status(HttpStatus.OK).send(editedTransaction);
		}

		throw new HttpException('Something wrong! Can\'t find transaction', HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@UseGuards(AuthGuard('jwt'))
	@Delete()
	public async deleteTransaction(@Res() res: any, @Body('id') id: string, @Body('walletId') walletId: string): Promise<any> {
		const isDeleted = await this.transactionService.delete(id, walletId);

		if (isDeleted) {
			return res.status(HttpStatus.OK).send('Successfully deleted');
		}

		throw new HttpException('Something wrong! Can\'t find transaction', HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
