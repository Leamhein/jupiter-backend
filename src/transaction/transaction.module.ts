import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HelperModule } from '../helper/helper.module';
import { TransactionController } from './transaction.controller';
import { TransactionSchema } from './transaction.schema';
import { TransactionService } from './transaction.service';
import { WalletsModule } from '../wallets/wallets.module';

@Module({
	imports: [
		MongooseModule.forFeature([{ name: 'Transactions', schema: TransactionSchema }]),
		forwardRef(() => WalletsModule),
		WalletsModule,
		HelperModule,
	],
	controllers: [TransactionController],
	providers: [TransactionService],
	exports: [TransactionService],
})
export class TransactionModule { }
