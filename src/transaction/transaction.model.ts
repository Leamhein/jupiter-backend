import { Wallet } from '../wallets/wallets.model';

export class Transaction {
	public _id: string | null = null;
	public walletId: string | Wallet;
	public category: string;
	public amount: number;
	public date: number;
	public note?: string;

	constructor(params: Partial<Transaction>) {
		Object.assign(this, params);
	}
}
