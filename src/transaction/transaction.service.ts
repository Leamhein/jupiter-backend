import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Transaction } from './transaction.model';
import { BaseService } from '../models/base-service';

@Injectable()
export class TransactionService extends BaseService<Transaction> {
	protected readonly parentIdFieldName = 'walletId';
	constructor(
		@InjectModel('Transactions') readonly transactionModel: Model<Transaction>,
	) {
		super(transactionModel);
	}
}
