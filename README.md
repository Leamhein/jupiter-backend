# Types:
**User:**

    {
    	username: string,
    	password: string,
    	email: string,
    }
**Bookkeeping:**

    {
    	userId:  string;
    	title:  string;
    	description:  string;
    	icon:  string;
    	currency:  string;
    }

**Wallet:**

    {
    	bookkeepingId:  string;
    	title:  string;
    	balance:  number;
    	icon:  string;
    }

**Transaction:**

    {
    	walletId:  string;
    	category:  string;
    	amount:  number;
    	date:  number;
    	note?:  string;
    }

# **Server API:**

**Endpoints:**
 - `/user`
 - `/bookkeeping`
 - `/wallets`
 - `/transaction`

**Add User:**
Endpoint: `/auth/register`
Method: `POST`
Headers: `Content-Type: application/x-www-form-urlencoded`
Body: `{ username: String, password: String, email: String }`

Response: `{ "success": true, "message": "Account created successfully" }`

**Auth user:**
Endpoint: `/auth/login`
Method: `POST`
Headers: `Content-Type: application/x-www-form-urlencoded`
Body: `{ username: String, password: String }`

Response: `{ "success": true, "message": "Token granted", "token": string, "user_id": string }`

**Add Bookkeeping/Wallet/Transaction:**
Endpoint: `/bookkeeping or /wallets or /transaction`
Method: `POST`
Headers: `Content-Type: application/x-www-form-urlencoded`
Authorization: `Bearer Token: string`
Body: `current class`

Response: `{ "success": true, "message": "**** successfully added", ${current class}`

**Get Bookkeeping/Wallet/Transaction by ID:**
Endpoint: `/bookkeeping or /wallets or /transaction`
Method: `GET`
Params: `id: string`
Authorization: `Bearer Token: string`

Response: `{ "success": true, ${current class}`

**Get all Bookkeeping/Wallet/Transaction by parent ID:**
Endpoint: `/bookkeeping or /wallets or /transaction`
Method: `GET`
Parents: `Bookkeeping: user
		Wallet: bookkeeping
		Transaction: wallet`
Params: `${parent}Id: string`
Authorization: `Bearer Token: string`

Response: `{ "success": true, [${current class}]`

**Edit Bookkeeping/Wallet/Transaction:**
Endpoint: `/bookkeeping or /wallets or /transaction`
Method: `PUT`
Headers: `Content-Type: application/x-www-form-urlencoded`
Authorization: `Bearer Token: string`
Body: `current class`

Response: `{ "success": true, ${current class}`

**Delete Bookkeeping/Wallet/Transaction:**
Endpoint: `/bookkeeping or /wallets or /transaction`
Method: `DELETE`
Headers: `Content-Type: application/x-www-form-urlencoded`
Authorization: `Bearer Token: string`
Body: `{
		${parent}Id: string,
		id: string,
	}`

Response: `{ "success": true, "message": "Removed successfully" }`
