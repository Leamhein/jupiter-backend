const parsedUrl = new URL(window.location.href);
const token = parsedUrl.searchParams.get('token');
const sendBtn = document.getElementById('send-btn');
const input = document.getElementById('input') as HTMLInputElement;
const secondInput = document.getElementById('second-input') as HTMLInputElement;
const alertElement = document.getElementById('alert');

enum PasswordStatuses {
	'PasswordsNotMatchOrShort' = 'Passwords must match or be at least 5 characters long',
	'PasswordChangeFailed' = 'Password Change Failed',
	'PasswordSuccessfullyChanged' = 'Password Successfully Changed',
}

sendBtn.onclick = () => {
	const inputValue = input.value;
	const secondInputValue = secondInput.value;
	const isNewPasswordValid = isPasswordValid(inputValue, secondInputValue);

	if (isNewPasswordValid) {
		sendNewPassword(token, inputValue, alertElement);
	} else {
		showAlert(alertElement, PasswordStatuses.PasswordsNotMatchOrShort);
	}
};

function isPasswordValid(inputValue: string, secondInputValue: string): boolean {
	if (inputValue !== secondInputValue || inputValue.length < 5) {
		return false;
	}

	return true;
}

function showAlert(alertElement: HTMLElement, status: PasswordStatuses, isError: boolean = true): void {
	const textElement = document.createElement('p');

	alertElement.setAttribute('class', 'alert-wrapper');

	if (isError) {
		alertElement.classList.add('alert-fail');
	} else {
		alertElement.classList.add('alert-success');
	}

	alertElement.innerHTML = '';
	textElement.innerHTML = status;
	alertElement.appendChild(textElement);
}

function sendNewPassword(token: string, password: string, alertElement: HTMLElement): void {
	const myHeaders = new Headers();
	myHeaders.append('Content-Type', 'application/json');

	const myRequest = new Request(window.location.href, {
		method: 'PUT',
		headers: myHeaders,
		body: JSON.stringify({
			password,
		}),
		mode: 'cors',
	});

	fetch(myRequest).then((response) => {
		if (response.status === 200) {
			showAlert(alertElement, PasswordStatuses.PasswordSuccessfullyChanged, false);
		} else {
			showAlert(alertElement, PasswordStatuses.PasswordChangeFailed);
		}
		return response.json();
	});
}
